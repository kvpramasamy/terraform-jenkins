terraform {
  backend "s3" {
    bucket  = "amybucket1nov"
    key     = "dev/terraform.tfstate"
    region  = "ap-south-1"
    encrypt = true
    dynamodb_table = "test"

  }
}
